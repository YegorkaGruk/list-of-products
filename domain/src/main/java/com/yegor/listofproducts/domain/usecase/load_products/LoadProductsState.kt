package com.yegor.listofproducts.domain.usecase.load_products

import com.yegor.listofproducts.domain.entity.ProductsEntity

sealed class LoadProductsState {

    object StateProcessing : LoadProductsState()

    data class StateSuccess(val result: ProductsEntity) : LoadProductsState()

    data class StateError(val error: Throwable) : LoadProductsState()
}