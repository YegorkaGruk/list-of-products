package com.yegor.listofproducts.domain.usecase.base

import io.reactivex.Single

interface SingleUseCase<Results, Params> : BaseUseCase {
    fun execute(params: Params): Single<Results>
}