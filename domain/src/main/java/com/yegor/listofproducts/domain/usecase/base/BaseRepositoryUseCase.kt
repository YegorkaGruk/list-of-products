package com.yegor.listofproducts.domain.usecase.base

import com.yegor.listofproducts.domain.executor.PostExecutionThread
import com.yegor.listofproducts.domain.executor.ThreadExecutor
import io.reactivex.Scheduler

abstract class BaseRepositoryUseCase(
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) {

    protected val threadExecutorScheduler: Scheduler
    protected val postExecutionThreadScheduler: Scheduler

    init {
        threadExecutorScheduler = threadExecutor.provideScheduler()
        postExecutionThreadScheduler = postExecutionThread.provideScheduler()
    }
}