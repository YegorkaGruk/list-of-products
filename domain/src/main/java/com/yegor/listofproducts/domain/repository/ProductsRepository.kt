package com.yegor.listofproducts.domain.repository

import com.yegor.listofproducts.domain.entity.ProductDetailsEntity
import com.yegor.listofproducts.domain.entity.ProductsEntity
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

interface ProductsRepository {

    fun loadProducts(): Single<ProductsEntity>

    fun loadProductsCache(): Maybe<ProductsEntity>

    fun saveProductsCache(products: ProductsEntity): Completable

    fun loadProductDetails(productId: String): Single<ProductDetailsEntity>

    fun loadProductDetailsCache(productId: String): Maybe<ProductDetailsEntity>

    fun saveProductDetailsCache(product: ProductDetailsEntity): Completable
}