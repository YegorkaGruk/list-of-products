package com.yegor.listofproducts.domain.entity

data class ProductsEntity(
    val products: List<ProductEntity>
)