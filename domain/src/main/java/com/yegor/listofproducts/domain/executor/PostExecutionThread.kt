package com.yegor.listofproducts.domain.executor

import io.reactivex.Scheduler

interface PostExecutionThread {
    fun provideScheduler(): Scheduler
}