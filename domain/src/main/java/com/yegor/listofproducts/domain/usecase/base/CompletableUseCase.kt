package com.yegor.listofproducts.domain.usecase.base

import io.reactivex.Completable

interface CompletableUseCase<Params> : BaseUseCase {
    fun execute(params: Params): Completable
}