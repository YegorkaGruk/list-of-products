package com.yegor.listofproducts.domain.usecase.base

import io.reactivex.Observable

interface ObservableUseCase<Results, Params> : BaseUseCase {
    fun execute(params: Params): Observable<Results>
}