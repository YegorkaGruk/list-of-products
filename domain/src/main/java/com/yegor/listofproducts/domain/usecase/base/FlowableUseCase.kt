package com.yegor.listofproducts.domain.usecase.base

import androidx.annotation.Nullable
import io.reactivex.Flowable

interface FlowableUseCase<Results, Params> : BaseUseCase {
    fun execute(@Nullable params: Params): Flowable<Results>
}