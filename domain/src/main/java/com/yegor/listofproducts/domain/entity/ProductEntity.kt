package com.yegor.listofproducts.domain.entity

data class ProductEntity(
    val image: String,
    val name: String,
    val price: Int,
    val productId: String
)