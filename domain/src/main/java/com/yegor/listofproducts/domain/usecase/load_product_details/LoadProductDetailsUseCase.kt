package com.yegor.listofproducts.domain.usecase.load_product_details

import com.yegor.listofproducts.domain.entity.ProductDetailsEntity
import com.yegor.listofproducts.domain.executor.PostExecutionThread
import com.yegor.listofproducts.domain.executor.ThreadExecutor
import com.yegor.listofproducts.domain.repository.ProductsRepository
import com.yegor.listofproducts.domain.usecase.base.FlowableUseCase
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Scheduler
import javax.inject.Inject

class LoadProductDetailsUseCase @Inject constructor(
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread,
    private val repository: ProductsRepository
) : FlowableUseCase<LoadProductDetailsState, String> {

    private val threadExecutorScheduler: Scheduler = threadExecutor.provideScheduler()
    private val postExecutionThreadScheduler: Scheduler = postExecutionThread.provideScheduler()

    override fun execute(productId: String): Flowable<LoadProductDetailsState> {

        val loadProductDetailsCacheMaybe = repository.loadProductDetailsCache(productId)
            .map { LoadProductDetailsState.StateSuccess(it) as LoadProductDetailsState }
            .onErrorReturn { LoadProductDetailsState.StateError(it) }

        val loadProductDetailsAndSaveMaybe = loadProductDetailsAndSave(productId)
            .map { LoadProductDetailsState.StateSuccess(it) as LoadProductDetailsState }
            .onErrorReturn { LoadProductDetailsState.StateError(it) }

        return loadProductDetailsCacheMaybe.mergeWith(loadProductDetailsAndSaveMaybe)
            .startWith(LoadProductDetailsState.StateProcessing)
            .subscribeOn(threadExecutorScheduler)
            .observeOn(postExecutionThreadScheduler)
    }

    private fun loadProductDetailsAndSave(productId: String): Maybe<ProductDetailsEntity> {
        return repository.loadProductDetails(productId)
            .flatMap { repository.saveProductDetailsCache(it).toSingleDefault(it) }
            .toMaybe()
    }
}