package com.yegor.listofproducts.domain.usecase.load_product_details

import com.yegor.listofproducts.domain.entity.ProductDetailsEntity

sealed class LoadProductDetailsState {

    object StateProcessing : LoadProductDetailsState()

    data class StateSuccess(val result: ProductDetailsEntity) : LoadProductDetailsState()

    data class StateError(val error: Throwable) : LoadProductDetailsState()
}