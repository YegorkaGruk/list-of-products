package com.yegor.listofproducts.domain.entity

data class ProductDetailsEntity(
    val description: String,
    val image: String,
    val name: String,
    val price: Int,
    val productId: String
)