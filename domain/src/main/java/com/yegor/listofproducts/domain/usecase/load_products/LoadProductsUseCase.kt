package com.yegor.listofproducts.domain.usecase.load_products

import com.yegor.listofproducts.domain.entity.ProductsEntity
import com.yegor.listofproducts.domain.executor.PostExecutionThread
import com.yegor.listofproducts.domain.executor.ThreadExecutor
import com.yegor.listofproducts.domain.repository.ProductsRepository
import com.yegor.listofproducts.domain.usecase.base.FlowableUseCase
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Scheduler
import javax.inject.Inject

class LoadProductsUseCase @Inject constructor(
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread,
    private val repository: ProductsRepository
) : FlowableUseCase<LoadProductsState, Void?> {

    private val threadExecutorScheduler: Scheduler = threadExecutor.provideScheduler()
    private val postExecutionThreadScheduler: Scheduler = postExecutionThread.provideScheduler()

    override fun execute(request: Void?): Flowable<LoadProductsState> {

        val loadProductsCacheMaybe = repository.loadProductsCache()
            .map { LoadProductsState.StateSuccess(it) as LoadProductsState }
            .onErrorReturn { LoadProductsState.StateError(it) }

        val loadProductsAndSaveMaybe = loadProductsAndSave()
            .map { LoadProductsState.StateSuccess(it) as LoadProductsState }
            .onErrorReturn { LoadProductsState.StateError(it) }

        return loadProductsCacheMaybe.mergeWith(loadProductsAndSaveMaybe)
            .startWith(LoadProductsState.StateProcessing)
            .subscribeOn(threadExecutorScheduler)
            .observeOn(postExecutionThreadScheduler)
    }

    private fun loadProductsAndSave(): Maybe<ProductsEntity> {
        return repository.loadProducts()
            .flatMap { repository.saveProductsCache(it).toSingleDefault(it) }
            .toMaybe()
    }
}