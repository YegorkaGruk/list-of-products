package com.yegor.listofproducts.domain.executor

import io.reactivex.Scheduler

interface ThreadExecutor {
    fun provideScheduler(): Scheduler
}