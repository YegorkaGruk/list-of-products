package com.yegor.listofproducts.domain.exception

import java.io.IOException

private const val MESSAGE = "No internet connection"

class NoInternetConnectionException : IOException(MESSAGE)