package com.yegor.listofproducts.presentation.screen.master.adapter

interface ProductsClickListener {
    fun onProductClicked(product: ProductItem, position: Int)
}