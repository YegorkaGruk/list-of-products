package com.yegor.listofproducts.presentation.injection.module

import com.yegor.listofproducts.domain.executor.PostExecutionThread
import com.yegor.listofproducts.domain.executor.ThreadExecutor
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton

@Module
abstract class DomainModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        @Singleton
        fun providesThreadExecutor(): ThreadExecutor {
            return object : ThreadExecutor {
                override fun provideScheduler(): Scheduler {
                    return Schedulers.io()
                }
            }
        }

        @Provides
        @JvmStatic
        @Singleton
        fun providesPostExecutionThread(): PostExecutionThread {
            return object : PostExecutionThread {
                override fun provideScheduler(): Scheduler {
                    return AndroidSchedulers.mainThread()
                }
            }
        }
    }
}