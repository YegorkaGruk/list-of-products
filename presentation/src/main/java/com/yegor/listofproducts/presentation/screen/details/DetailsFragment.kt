package com.yegor.listofproducts.presentation.screen.details

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import coil.api.load
import com.google.android.material.snackbar.Snackbar
import com.yegor.listofproducts.domain.entity.ProductDetailsEntity
import com.yegor.listofproducts.domain.usecase.load_product_details.LoadProductDetailsState
import com.yegor.listofproducts.presentation.R
import com.yegor.listofproducts.presentation.base.BaseFragment
import com.yegor.listofproducts.presentation.screen.details.viewmodel.DetailsViewModel
import com.yegor.listofproducts.presentation.screen.details.viewmodel.DetailsViewModelFactory
import timber.log.Timber
import javax.inject.Inject

class DetailsFragment : BaseFragment<DetailsViewModel>() {

    @Inject
    lateinit var vmFactory: DetailsViewModelFactory

    private lateinit var productImage: ImageView
    private lateinit var productName: TextView
    private lateinit var productPrice: TextView
    private lateinit var productDescription: TextView

    private var snackBar: Snackbar? = null

    override fun layoutId() = R.layout.fragment_details

    override fun getViewModelClass() = DetailsViewModel::class.java

    override fun getViewModelFactory() = vmFactory

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        productImage = view.findViewById(R.id.product_image)
        productName = view.findViewById(R.id.product_name)
        productPrice = view.findViewById(R.id.product_price)
        productDescription = view.findViewById(R.id.product_description)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val productId = getProductId()
        if (productId == null) {
            Toast.makeText(activity, "arguments == null", Toast.LENGTH_SHORT).show()
            return
        }

        observeProductDetails()
        viewModel.loadProducts(productId)
    }

    override fun onStop() {
        super.onStop()
        snackBar?.dismiss()
    }

    private fun observeProductDetails() {
        val productsObserver = Observer<LoadProductDetailsState> { event ->
            if (event is LoadProductDetailsState.StateSuccess) {
                setUpViews(event.result)
            } else if (event is LoadProductDetailsState.StateError) {
                Timber.w(event.error)
                showReloadSnackBar()
            }
        }
        viewModel.productDetailsLiveData.observe(viewLifecycleOwner, productsObserver)
    }

    private fun getProductId(): String? {
        return arguments?.run { DetailsFragmentArgs.fromBundle(this).productId }
    }

    private fun setUpViews(productDetails: ProductDetailsEntity) {
        productImage.load(productDetails.image) {
            crossfade(true)
        }
        productName.text = productDetails.name
        productPrice.text = productDetails.price.toString()
        productDescription.text = productDetails.description
    }

    private fun showReloadSnackBar() {
        val productId = getProductId() ?: return
        val textId = R.string.failed_to_load_product_details
        val snackBar = Snackbar.make(productName, textId, Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.reload) { viewModel.loadProducts(productId) }
        snackBar.show()
        this.snackBar = snackBar
    }
}
