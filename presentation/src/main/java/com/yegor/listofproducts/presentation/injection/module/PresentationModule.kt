package com.yegor.listofproducts.presentation.injection.module

import com.yegor.listofproducts.presentation.MainActivity
import com.yegor.listofproducts.presentation.screen.details.DetailsFragment
import com.yegor.listofproducts.presentation.screen.master.MasterFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class PresentationModule {

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributeMasterFragment(): MasterFragment

    @ContributesAndroidInjector
    abstract fun contributeDetailsFragment(): DetailsFragment
}