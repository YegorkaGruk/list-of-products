package com.yegor.listofproducts.presentation.utils

const val PRODUCT_IMAGE_NAVIGATOR_EXTRA = "productImage"
const val PRODUCT_NAME_NAVIGATOR_EXTRA = "productName"
const val PRODUCT_PRICE_NAVIGATOR_EXTRA = "productPrice"