package com.yegor.listofproducts.presentation.screen.master.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.yegor.listofproducts.presentation.R

class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val productImage = itemView.findViewById<ImageView>(R.id.product_image)
    private val productName = itemView.findViewById<TextView>(R.id.product_name)
    private val productPrice = itemView.findViewById<TextView>(R.id.product_price)

    fun bind(product: ProductItem, clickListener: ProductsClickListener) {
        productImage.load(product.image) {
            crossfade(true)
        }
        productName.text = product.name
        productPrice.text = product.price.toString()
        itemView.setOnClickListener {
            clickListener.onProductClicked(product, adapterPosition)
        }
    }
}