package com.yegor.listofproducts.presentation.injection.module

import android.content.Context
import com.yegor.listofproducts.data.repository.ProductsRepositoryImpl
import com.yegor.listofproducts.data.service.database.ProductDao
import com.yegor.listofproducts.data.service.database.ProductsDataBase
import com.yegor.listofproducts.data.service.rest.APIServiceFactory
import com.yegor.listofproducts.data.service.rest.ProductsService
import com.yegor.listofproducts.data.store.products_api.ProductsApiStore
import com.yegor.listofproducts.data.store.products_api.ProductsApiStoreImpl
import com.yegor.listofproducts.data.store.products_cache.ProductsCacheStore
import com.yegor.listofproducts.data.store.products_cache.ProductsCacheStoreImpl
import com.yegor.listofproducts.domain.repository.ProductsRepository
import com.yegor.listofproducts.presentation.BuildConfig
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class DataModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        @Singleton
        fun provideProductsService(): ProductsService {
            return APIServiceFactory.makeRestService(BuildConfig.DEBUG)
        }

        @Provides
        @JvmStatic
        @Singleton
        fun provideDataBase(context: Context): ProductsDataBase = ProductsDataBase.getInstance(context)

        @Provides
        @JvmStatic
        @Singleton
        fun provideProfileDao(dataBase: ProductsDataBase): ProductDao = dataBase.productDao()
    }

    @Binds
    @Singleton
    abstract fun bindProductsApiStore(store: ProductsApiStoreImpl): ProductsApiStore

    @Binds
    @Singleton
    abstract fun bindProductsCacheStore(store: ProductsCacheStoreImpl): ProductsCacheStore

    @Binds
    @Singleton
    abstract fun bindProductsRepository(repository: ProductsRepositoryImpl): ProductsRepository
}