package com.yegor.listofproducts.presentation.screen.master.adapter

import com.yegor.listofproducts.domain.entity.ProductEntity

class ProductItem(
    val image: String,
    val name: String,
    val price: Int,
    val productId: String
) {
    constructor(entity: ProductEntity) : this(
        entity.image,
        entity.name,
        entity.price,
        entity.productId
    )
}