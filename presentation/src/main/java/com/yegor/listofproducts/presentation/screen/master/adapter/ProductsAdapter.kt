package com.yegor.listofproducts.presentation.screen.master.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.yegor.listofproducts.domain.entity.ProductEntity
import com.yegor.listofproducts.presentation.R

class ProductsAdapter(
    private val clickListener: ProductsClickListener
) : RecyclerView.Adapter<ProductViewHolder>() {

    private val products = arrayListOf<ProductItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_product, parent, false)
        return ProductViewHolder(view)
    }

    override fun getItemCount() = products.size

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val product = products[position]
        holder.bind(product, clickListener)
    }

    fun updateProducts(newProductsEntities: List<ProductEntity>) {

        val newProducts = newProductsEntities.map { ProductItem(it) }

        val productsDiffUtilCallback = ProductsDiffUtilCallback(products, newProducts)
        val productDiffResult = DiffUtil.calculateDiff(productsDiffUtilCallback)

        products.clear()
        products.addAll(newProducts)

        productDiffResult.dispatchUpdatesTo(this)
    }
}