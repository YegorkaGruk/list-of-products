package com.yegor.listofproducts.presentation.screen.master

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.yegor.listofproducts.domain.usecase.load_products.LoadProductsState
import com.yegor.listofproducts.presentation.R
import com.yegor.listofproducts.presentation.base.BaseFragment
import com.yegor.listofproducts.presentation.screen.master.adapter.ProductItem
import com.yegor.listofproducts.presentation.screen.master.adapter.ProductsAdapter
import com.yegor.listofproducts.presentation.screen.master.adapter.ProductsClickListener
import com.yegor.listofproducts.presentation.screen.master.viewmodel.MasterViewModel
import com.yegor.listofproducts.presentation.screen.master.viewmodel.MasterViewModelFactory
import timber.log.Timber
import javax.inject.Inject

class MasterFragment : BaseFragment<MasterViewModel>(), ProductsClickListener {

    companion object {
        const val SPAN_COUNT = 2
    }

    @Inject
    lateinit var vmFactory: MasterViewModelFactory

    private lateinit var productsList: RecyclerView

    private var snackBar: Snackbar? = null

    override fun layoutId() = R.layout.fragment_master

    override fun getViewModelClass() = MasterViewModel::class.java

    override fun getViewModelFactory() = vmFactory

    private val productsAdapter = ProductsAdapter(this)

    override fun onProductClicked(product: ProductItem, position: Int) {
        val view = view ?: return
        val productId = product.productId
        val action = MasterFragmentDirections.actionMasterFragmentToDetailsFragment(productId)
        Navigation.findNavController(view).navigate(action)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        productsList = view.findViewById(R.id.products_list)
        productsList.layoutManager = GridLayoutManager(activity, SPAN_COUNT)
        productsList.adapter = productsAdapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val productsObserver = Observer<LoadProductsState> { event ->
            if (event is LoadProductsState.StateSuccess) {
                productsAdapter.updateProducts(event.result.products)
            } else if (event is LoadProductsState.StateError) {
                Timber.w(event.error)
                showReloadSnackBar()
            }
        }
        viewModel.productsLiveData.observe(viewLifecycleOwner, productsObserver)
    }

    override fun onStop() {
        super.onStop()
        snackBar?.dismiss()
    }

    private fun showReloadSnackBar() {
        val textId = R.string.failed_to_load_products
        val snackBar = Snackbar.make(productsList, textId, Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.reload) { viewModel.loadProducts() }
        snackBar.show()
        this.snackBar = snackBar
    }
}
