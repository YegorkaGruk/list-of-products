package com.yegor.listofproducts.presentation.screen.details.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yegor.listofproducts.domain.usecase.load_product_details.LoadProductDetailsUseCase
import javax.inject.Inject

class DetailsViewModelFactory @Inject constructor(
    private val loadProductDetailsUseCase: LoadProductDetailsUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(DetailsViewModel::class.java)) {
            DetailsViewModel(loadProductDetailsUseCase) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}