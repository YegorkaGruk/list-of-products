package com.yegor.listofproducts.presentation.screen.master.viewmodel

import androidx.lifecycle.LiveData
import com.yegor.listofproducts.domain.usecase.load_products.LoadProductsState
import com.yegor.listofproducts.domain.usecase.load_products.LoadProductsUseCase
import com.yegor.listofproducts.presentation.base.BaseViewModel
import com.yegor.listofproducts.presentation.base.SingleLiveEvent
import io.reactivex.disposables.Disposable

class MasterViewModel(private val loadProductsUseCase: LoadProductsUseCase) : BaseViewModel() {

    private val _productsLiveData = SingleLiveEvent<LoadProductsState>()

    val productsLiveData: LiveData<LoadProductsState> = _productsLiveData

    private var disposable: Disposable? = null

    override fun onStart() {
        super.onStart()
        loadProducts()
    }

    override fun onStop() {
        super.onStop()
        disposable?.dispose()
    }

    fun loadProducts() {
        disposable = loadProductsUseCase.execute(null)
            .subscribe { _productsLiveData.value = it }
    }
}
