package com.yegor.listofproducts.presentation.screen.master.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yegor.listofproducts.domain.usecase.load_products.LoadProductsUseCase
import javax.inject.Inject

class MasterViewModelFactory @Inject constructor(
    private val loadProductsUseCase: LoadProductsUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(MasterViewModel::class.java)) {
            MasterViewModel(loadProductsUseCase) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}