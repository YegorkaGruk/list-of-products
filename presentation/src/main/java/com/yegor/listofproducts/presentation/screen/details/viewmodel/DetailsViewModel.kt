package com.yegor.listofproducts.presentation.screen.details.viewmodel

import androidx.lifecycle.LiveData
import com.yegor.listofproducts.domain.usecase.load_product_details.LoadProductDetailsState
import com.yegor.listofproducts.domain.usecase.load_product_details.LoadProductDetailsUseCase
import com.yegor.listofproducts.presentation.base.BaseViewModel
import com.yegor.listofproducts.presentation.base.SingleLiveEvent
import io.reactivex.disposables.Disposable

class DetailsViewModel(
    private val loadProductDetailsUseCase: LoadProductDetailsUseCase
) : BaseViewModel() {

    private val _productDetailsLiveData = SingleLiveEvent<LoadProductDetailsState>()

    val productDetailsLiveData: LiveData<LoadProductDetailsState> = _productDetailsLiveData

    private var disposable: Disposable? = null

    override fun onStop() {
        super.onStop()
        disposable?.dispose()
    }

    fun loadProducts(productId: String) {
        disposable = loadProductDetailsUseCase.execute(productId)
            .subscribe { _productDetailsLiveData.value = it }
    }
}
