package com.yegor.listofproducts.data

const val BASE_URL = "https://s3-eu-west-1.amazonaws.com"

const val LIST_PATH = "developer-application-test/cart/list"
const val DETAILS_PATH = "developer-application-test/cart/{id}/detail"

const val ID_PARAM = "id"
