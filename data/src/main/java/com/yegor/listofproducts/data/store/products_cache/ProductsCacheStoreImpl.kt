package com.yegor.listofproducts.data.store.products_cache

import com.yegor.listofproducts.data.service.database.ProductDao
import com.yegor.listofproducts.data.service.database.model.ProductDb
import io.reactivex.Completable
import io.reactivex.Maybe
import javax.inject.Inject

class ProductsCacheStoreImpl @Inject constructor(
    private val productDao: ProductDao
) : ProductsCacheStore {

    override fun loadProductsCache(): Maybe<List<ProductDb>> {
        return productDao.getProducts()
    }

    override fun saveProductsCache(products: List<ProductDb>): Completable {
        return Completable.fromAction { productDao.insertOrUpdateProducts(products) }
    }

    override fun loadProductDetailsCache(productId: String): Maybe<ProductDb> {
        return productDao.getProductById(productId)
    }

    override fun saveProductDetailsCache(product: ProductDb): Completable {
        return Completable.fromAction { productDao.insertOrUpdateProduct(product) }
    }
}