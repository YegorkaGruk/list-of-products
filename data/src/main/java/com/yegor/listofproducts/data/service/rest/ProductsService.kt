package com.yegor.listofproducts.data.service.rest

import com.yegor.listofproducts.data.DETAILS_PATH
import com.yegor.listofproducts.data.ID_PARAM
import com.yegor.listofproducts.data.LIST_PATH
import com.yegor.listofproducts.data.service.rest.model.response.ProductDetailsResponse
import com.yegor.listofproducts.data.service.rest.model.response.ProductsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface ProductsService {

    @GET(LIST_PATH)
    fun loadCurrencies(): Single<ProductsResponse>

    @GET(DETAILS_PATH)
    fun loadProductDetails(@Path(ID_PARAM) productId: String): Single<ProductDetailsResponse>
}