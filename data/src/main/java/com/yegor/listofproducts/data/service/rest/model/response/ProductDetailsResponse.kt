package com.yegor.listofproducts.data.service.rest.model.response

import com.google.gson.annotations.SerializedName
import com.yegor.listofproducts.domain.entity.ProductDetailsEntity

class ProductDetailsResponse(
    @SerializedName("description")
    val description: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("price")
    val price: Int,
    @SerializedName("product_id")
    val productId: String
) {

    fun toEntity(): ProductDetailsEntity {
        return ProductDetailsEntity(description, image, name, price, productId)
    }
}