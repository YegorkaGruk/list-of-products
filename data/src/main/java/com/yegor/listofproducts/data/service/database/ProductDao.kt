package com.yegor.listofproducts.data.service.database

import androidx.room.*
import com.yegor.listofproducts.data.service.database.model.ProductDb
import io.reactivex.Maybe

@Dao
interface ProductDao {

    @Query("select * from product")
    fun getProducts(): Maybe<List<ProductDb>>

    @Deprecated("")
    @Query("select * from product")
    fun getProductsPlaint(): List<ProductDb>

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdateProducts(products: List<ProductDb>) {
        products.forEach { insertOrUpdateProduct(it) }
    }

    fun insertOrUpdateProduct(product: ProductDb) {
        val newProduct = getProductByIdStraightforward(product.productId)
        val productCopy = newProduct?.copy(
            image = product.image,
            name = product.name,
            price = product.price,
            description = product.description
        )
        if (productCopy != null) {
            insertProduct(productCopy)
        } else {
            insertProduct(product)
        }
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProduct(product: ProductDb)

    @Query("select * from product where productId = :productId  LIMIT 1")
    fun getProductByIdStraightforward(productId: String): ProductDb?

    @Query("select * from product where productId = :productId  LIMIT 1")
    fun getProductById(productId: String): Maybe<ProductDb>
}