package com.yegor.listofproducts.data.service.rest

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.yegor.listofproducts.data.BASE_URL
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object APIServiceFactory {

    private const val CONNECT_TIMEOUT = 45L
    private const val READ_TIMEOUT = 30L

    inline fun <reified T> makeRestService(isDebug: Boolean): T {
        val loggingInterceptor = makeLoggingInterceptor(isDebug)
        val okHttpClient = makeOkHttpClient(arrayOf(loggingInterceptor))
        val gson = makeGson()
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        return retrofit.create(T::class.java)
    }

    fun makeOkHttpClient(interceptors: Array<Interceptor>): OkHttpClient {
        val okHttpClientBuilder = OkHttpClient.Builder()
            .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
        interceptors.forEach { okHttpClientBuilder.addInterceptor(it) }
        return okHttpClientBuilder.build()
    }

    fun makeLoggingInterceptor(isDebug: Boolean): Interceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = if (isDebug) {
            HttpLoggingInterceptor.Level.BODY
        } else {
            HttpLoggingInterceptor.Level.NONE
        }
        return logging
    }

    fun makeGson(): Gson {
        return GsonBuilder()
            .setLenient()
            .create()
    }
}