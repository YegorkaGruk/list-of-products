package com.yegor.listofproducts.data.service.database.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.yegor.listofproducts.domain.entity.ProductDetailsEntity
import com.yegor.listofproducts.domain.entity.ProductEntity

@Entity(tableName = "product")
data class ProductDb(
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    @ColumnInfo(name = "image")
    val image: String,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "price")
    val price: Int,
    @ColumnInfo(name = "productId")
    val productId: String,
    @ColumnInfo(name = "description")
    val description: String?
) {
    constructor(product: ProductEntity) : this(
        null,
        product.image,
        product.name,
        product.price,
        product.productId,
        null
    )

    constructor(product: ProductDetailsEntity) : this(
        null,
        product.image,
        product.name,
        product.price,
        product.productId,
        product.description
    )

    fun toProductEntity() = ProductEntity(image, name, price, productId)

    fun toProductDetailsEntity(): ProductDetailsEntity {
        return ProductDetailsEntity(description ?: "", image, name, price, productId)
    }
}