package com.yegor.listofproducts.data.store.products_api

import com.yegor.listofproducts.data.service.rest.ProductsService
import com.yegor.listofproducts.data.service.rest.model.response.ProductDetailsResponse
import com.yegor.listofproducts.data.service.rest.model.response.ProductsResponse
import com.yegor.listofproducts.domain.exception.NoInternetConnectionException
import io.reactivex.Completable
import io.reactivex.Single
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import javax.inject.Inject

class ProductsApiStoreImpl @Inject constructor(
    private val service: ProductsService
) : ProductsApiStore {

    companion object {
        private const val TIMEOUT_MS = 1500
        private const val HOST_NAME = "8.8.8.8"
        private const val PORT = 53
    }

    override fun loadProducts(): Single<ProductsResponse> {
        return checkInternetConnection().andThen(service.loadCurrencies())
    }

    override fun loadProductDetails(productId: String): Single<ProductDetailsResponse> {
        return checkInternetConnection().andThen(service.loadProductDetails(productId))
    }

    private fun checkInternetConnection(): Completable {
        return Completable.fromAction {
            val socket = Socket()
            val socketAddress = InetSocketAddress(HOST_NAME, PORT)
            socket.connect(socketAddress, TIMEOUT_MS)
            socket.close()
        }.onErrorResumeNext {
            if (it is IOException) {
                Completable.error(NoInternetConnectionException())
            } else {
                Completable.error(it)
            }
        }
    }
}