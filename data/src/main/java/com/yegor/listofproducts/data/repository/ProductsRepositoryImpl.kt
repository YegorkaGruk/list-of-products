package com.yegor.listofproducts.data.repository

import com.yegor.listofproducts.data.service.database.model.ProductDb
import com.yegor.listofproducts.data.store.products_api.ProductsApiStore
import com.yegor.listofproducts.data.store.products_cache.ProductsCacheStore
import com.yegor.listofproducts.domain.entity.ProductDetailsEntity
import com.yegor.listofproducts.domain.entity.ProductEntity
import com.yegor.listofproducts.domain.entity.ProductsEntity
import com.yegor.listofproducts.domain.repository.ProductsRepository
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class ProductsRepositoryImpl @Inject constructor(
    private val apiStore: ProductsApiStore,
    private val cacheStore: ProductsCacheStore
) : ProductsRepository {

    override fun loadProducts(): Single<ProductsEntity> {
        return apiStore.loadProducts().map { it.toEntity() }
    }

    override fun loadProductsCache(): Maybe<ProductsEntity> {
        return cacheStore.loadProductsCache()
            .flattenAsObservable { it }
            .map { it.toProductEntity() }
            .collectInto(arrayListOf<ProductEntity>(), { list, product -> list.add(product) })
            .map { ProductsEntity(it) }
            .toMaybe()
    }

    override fun saveProductsCache(products: ProductsEntity): Completable {
        return Observable.fromIterable(products.products)
            .map { ProductDb(it) }
            .collectInto(arrayListOf<ProductDb>(), { list, product -> list.add(product) })
            .flatMapCompletable { cacheStore.saveProductsCache(it) }
    }

    override fun loadProductDetails(productId: String): Single<ProductDetailsEntity> {
        return apiStore.loadProductDetails(productId)
            .map { it.toEntity() }
    }

    override fun loadProductDetailsCache(productId: String): Maybe<ProductDetailsEntity> {
        return cacheStore.loadProductDetailsCache(productId)
            .map { it.toProductDetailsEntity() }
    }

    override fun saveProductDetailsCache(product: ProductDetailsEntity): Completable {
        return Single.just(product)
            .map { ProductDb(it) }
            .flatMapCompletable { cacheStore.saveProductDetailsCache(it) }
    }
}