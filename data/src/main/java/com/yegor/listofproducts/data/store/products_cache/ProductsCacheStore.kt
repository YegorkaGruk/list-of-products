package com.yegor.listofproducts.data.store.products_cache

import com.yegor.listofproducts.data.service.database.model.ProductDb
import io.reactivex.Completable
import io.reactivex.Maybe

interface ProductsCacheStore {

    fun loadProductsCache(): Maybe<List<ProductDb>>

    fun saveProductsCache(products: List<ProductDb>): Completable

    fun loadProductDetailsCache(productId: String): Maybe<ProductDb>

    fun saveProductDetailsCache(product: ProductDb): Completable
}