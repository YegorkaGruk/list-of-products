package com.yegor.listofproducts.data.service.rest.model.response

import com.google.gson.annotations.SerializedName
import com.yegor.listofproducts.domain.entity.ProductsEntity

class ProductsResponse(
    @SerializedName("products")
    val products: List<ProductResponse>
) {

    fun toEntity(): ProductsEntity {
        val products = products.map { it.toEntity() }
        return ProductsEntity(products)
    }
}