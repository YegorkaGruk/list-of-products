package com.yegor.listofproducts.data.store.products_api

import com.yegor.listofproducts.data.service.rest.model.response.ProductDetailsResponse
import com.yegor.listofproducts.data.service.rest.model.response.ProductsResponse
import io.reactivex.Single

interface ProductsApiStore {

    fun loadProducts(): Single<ProductsResponse>

    fun loadProductDetails(productId: String): Single<ProductDetailsResponse>
}