package com.yegor.listofproducts.data.service.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.yegor.listofproducts.data.service.database.model.ProductDb

@Database(
    entities = [ProductDb::class],
    version = 1,
    exportSchema = false
)
abstract class ProductsDataBase : RoomDatabase() {

    companion object {

        private const val DATABASE_NAME = "ProductsDataBase"

        @Volatile
        private var INSTANCE: ProductsDataBase? = null

        fun getInstance(context: Context): ProductsDataBase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also {
                    INSTANCE = it
                }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                ProductsDataBase::class.java,
                DATABASE_NAME
            )
                .fallbackToDestructiveMigration()
                .build()
    }

    abstract fun productDao(): ProductDao
}
